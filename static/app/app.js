(function(angular){
    //Kreiranje novog AngularJS modula pod nazivom app.
    //Ovaj modul zavisi od ui.router modula.
    var app = angular.module("app", ["ui.router"]);

    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
        /*
         * Stanje se definise preko state funkcije iz $stateProvider-a.
         * Funkciji state se prosledjuje objekat stanja koji sadrzi atribute stanja,
         * ili naziv stanja i potom objekat stanja.
         * Pozive state funkcija je moguce ulancavati jer svaka vraca $stateProvider objekat.
         */
        $stateProvider.state({
            name: "home", //Naziv stanja.
            url: "/", //URL koji se mapira na zadato stanje. Ovaj URL ce zapravo biti vidljiv kao identifikator fragmenta.
            templateUrl: "/app/components/naruci/naruci.tpl.html", //URL do sablona za generisanje prikaza.
            controller: "NaruciCtrl", //Kontroler koji je potrebno injektovati u prikaz.
            controllerAs: "nc" //Posto ce biti upotrebljen imenovani kontroler, navodi se i ime kontrolera.
        }).state("narucivanje", { 
            url: "/narucivanje",
            templateUrl: "/app/components/narucivanje/narucivanje.tpl.html",
            controller: "NarucivanjeCtrl",
            controllerAs: "nac"
        }).state("porucivanje", {
            url: "/porucivanje/{id_tipa_pica: int}", //Parametrizovana ruta, kljuc predstavlja naziv parametra a vrednost tip parametra.
            templateUrl: "/app/components/porucivanje/porucivanje.tpl.html",
            controller: "PorucivanjeCtrl",
            controllerAs: "pc"
        }).state("pica", {
            url: "/pica/:userName",
            templateUrl: "/app/components/pica/pica.tpl.html",
            controller: "PicaCtrl",
            controllerAs: "picac",
            params: {
                userName: null
            }
        }).state({
            name: "pice",
            url: "/pica/{id_pica: int}/:userName",
            templateUrl: "/app/components/pice/pice.tpl.html",
            controller: "PiceCtrl",
            controllerAs: "pc",
            params: {
                userName: null
            }
        }).state({
            name: "korisnici",
            url: "/korisnici/:userName",
            templateUrl: "/app/components/korisnici/korisnici.tpl.html",
            controller: "KorisniciCtrl",
            controllerAs: "kc",
            params: {
                userName: null
            }
        }).state({
            name: "korisnik",
            url: "/korisnici/:korisnicko_ime/:userName",
            templateUrl: "/app/components/korisnik/korisnik.tpl.html",
            controller: "KorisnikCtrl",
            controllerAs: "kc",
            params: {
                korisnicko_ime: null,
                userName: null
            }
        }).state({
            name: "login",
            url: "/login",
            templateUrl: "/app/components/login/login.tpl.html",
            controller: "LoginCtrl",
            controllerAs: "lc"
        }).state({
            name: "prikaz",
            url: "/prikaz",
            templateUrl: "/app/components/prikaz/prikaz.tpl.html",
            controller: "PrikazCtrl",
            controllerAs: "pc"
        }).state({
            name: "sank",
            url: "/sank/:userName",
            templateUrl: "/app/components/sank/sank.tpl.html",
            controller: "SankCtrl",
            controllerAs: "sc",
            params: {
                userName: null
            }
         })//.state({
        //     name: "narudzbenica",
        //     url: "/narudzbenica",
        //     templateUrl: "/app/components/narudzbenica/narudzbenica.tpl.html",
        //     controller: "NarudzbenicaCtrl",
        //     controllerAs: "nc"
        // })
        ;

        //Ukoliko zadata ruta ne odgovara ni jednoj od ruta stanja vrace se na link
        //za home stanje.
        $urlRouterProvider.otherwise("/");
    }]);
})(angular);