(function(angular) {
    angular.module("app").controller("PrikazCtrl", ["$stateParams", "$state",  "$http", function($stateParams, $state, $http) {
        var that = this;
        this.porudzbinaPrikaz = porudzbina;


        this.ukupnaSuma = 0;

        this.suma = function(){
            for (i = 0; i < porudzbina.length; i++){
                that.ukupnaSuma = that.ukupnaSuma + (porudzbina[i].kolicina * porudzbina[i].cena );
            }
        }


        this.clear = function(){
            porudzbina = [];
        }

        this.dobaviPorudzbinu = function(){

        };

        this.naruci = function(){
            if (porudzbina.length > 0){
                data = {
                    "stavke": porudzbina,
                    "ukupnaCena": that.ukupnaSuma
                };
                $http.post("/porudzbina", data).then(function(response){
                    that.dobaviPorudzbinu();
                    that.clear();
                }, function(){
                    console.log("Greska pri dodavanju korisnika! Kod: " + response.status);
                });
            } else {
                window.alert("Porudzbina je prazna :(.")
            }
        }

        this.suma();

    }]);
})(angular);