(function(angular) {
    angular.module("app").controller("PiceCtrl", ["$stateParams", "$state",  "$http", function($stateParams, $state, $http) {
        var that = this;
        this.pice = {};
        this.userName = $state.params.userName;
        this.tipPica = []

        this.dobaviPice = function(id_pica) {
            $http.get("/pica/"+id_pica).then(function(response) {
                that.pice = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju pica! Kod: " + response.status);
            });
        }

        this.dobaviTipovePica = function() {
            $http.get("/pica/tip").then(function(response){
                that.tipPica = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju tipova pica! Kod: " + response.status);
            })
        }

        this.izmeniPice = function() {
            $http.put("/pice/"+that.pice.id_pica, that.pice).then(
                function(response) {
                    that.dobaviPice(that.pice.id_pica);
                    $state.go("pica", {userName: that.userName});
                }, function(response) {
                    console.log("Greska pri izmeni pica");
                }
            )
        }
        this.dobaviPice($stateParams["id_pica"]);
        this.dobaviTipovePica();
    }]);
})(angular);