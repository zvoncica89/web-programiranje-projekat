(function(angular) {
    angular.module("app").controller("SankCtrl", ["$stateParams", "$state",  "$http", function($stateParams, $state, $http) {
        var that = this;

        this.porudzbine = [];

        this.dobaviPorudzbine = function() {
            $http.get("/sank/aktivnePorudzbine").then(function(response) {
                that.porudzbine = response.data;
            }, function(response) {
                $state.go("login",);
            });
        }

        this.logout = function() {
            $http.get("/logout/"+$state.params.userName).then(function(){
                // that.loggedIn = false;
                $state.go("login", {}, {reload: true});
            }, function() {

            })
        }

        
        this.obrisiPorudzbinu = function(id){
            $http.delete("/sank/"+id).then(function(response){
                that.dobaviPorudzbine();
            }, function(response){
                console.log("Greska pri uklanjanju porudzbine! Kod: " + response.status);
            });
        }

        this.gotovaPorudzbina = function(porudzbina){
            if (confirm("Da li zelite da zakljucite porudzbinu?")) {
                porudzbina.korisnik = $stateParams.userName;
                $http.put("/sank", porudzbina).then(function(response){
                    that.dobaviPorudzbine();
                }, function(response){
                    console.log("Greska pri izmeni porudzbine! Kod: " + response.status);
                });
            }
        }

        this.dobaviPorudzbine();
    }]);
})(angular);