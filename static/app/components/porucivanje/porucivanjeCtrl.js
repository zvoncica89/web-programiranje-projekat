var porudzbina = [];
(function(angular) {
    angular.module("app").controller("PorucivanjeCtrl", ["$stateParams", "$http", function($stateParams, $http) {
        var that = this;
        this.pica = [];

        this.stavka = {
            kolicina : undefined,
            ime_pica : "",
            pice_id_pica : undefined,
            cena : undefined,
            cenaUkupna: undefined,
            narudzbina_id_narudzbine : undefined
        };

        // this.porudzbina = [];

        this.dobaviPica = function() {
            $http.get("/porucivanje/"+$stateParams.id_tipa_pica).then(function(response) {
                that.pica = response.data;
                that.pica.forEach(element => {
                    element.naruceno = 0;
                });
            }, function(response) {
                console.log(response.status);
            });
        }

        this.dodaj = function(pice){
            if (pice.naruceno != 0){
                that.stavka.kolicina = pice.naruceno;
                that.stavka.ime_pica = pice.ime_pica;
                that.stavka.pice_id_pica = pice.id_pica;
                that.stavka.cena = pice.cena;
                that.stavka.cenaUkupna = pice.naruceno * pice.cena;
                porudzbina.push(angular.copy(that.stavka));
            }
           
        }
        
        this.plus = function(pice){
           if (pice.naruceno < pice.kolicina){
            pice.naruceno++;
        }
        }
        this.minus = function(pice){
            pice.naruceno--;
        }

        this.clear = function(){
            that.stavka = {
                kolicina : undefined,
                ime_pica : "",
                pice_id_pica : undefined,
                cena : undefined,
                cenaUkupna : undefined,
                narudzbina_id_narudzbine : undefined
            };
            porudzbina = [];
        }

        this.dobaviPica();
    }]);
})(angular);