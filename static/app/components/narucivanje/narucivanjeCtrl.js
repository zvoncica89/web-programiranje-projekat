(function (angular) {
    var app = angular.module("app");
    
    app.controller("NarucivanjeCtrl", ["$stateParams", "$http", function($stateParams, $http) {
        var that = this;
        this.tip_pica = []
       
        this.dobaviTipovePica = function() {
            $http.get("/narucivanja").then(function(response){
                that.tip_pica = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju tipova pica! Kod: " + response.status);
            })
        }
        this.dobaviTipovePica();
}
    ]);
})(angular);