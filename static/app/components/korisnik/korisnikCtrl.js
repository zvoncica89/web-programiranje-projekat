(function(angular) {
    angular.module("app").controller("KorisnikCtrl", ["$stateParams", "$state", "$http", function($stateParams, $state, $http) {
        var that = this;
        this.korisnik = {};
        this.userName = $state.params.userName;

        this.dobaviKorisnika = function(korisnicko_ime) {
            $http.get("/korisnici/"+korisnicko_ime).then(function(response) {
                that.korisnik = response.data;
            }, function(response) {
                console.log(response.status);
            });
        }

        this.izmeniKorisnika = function() {
            $http.put("/korisnici/"+that.korisnik.korisnicko_ime, that.korisnik).then(
                function(response) {
                    that.dobaviKorisnika(that.korisnik.korisnicko_ime);
                    $state.go("korisnici", {userName: that.userName});
                }, function(response) {
                    console.log("Greska pri izmeni korisnika");
                }
            )
        }
        this.dobaviKorisnika($stateParams["korisnicko_ime"]);
    }]);
})(angular);