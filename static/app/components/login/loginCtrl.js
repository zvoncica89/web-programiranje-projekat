(function (angular) {
    var app = angular.module("app");
    
    app.controller("LoginCtrl", ["$http","$state", function ($http, $state) {
        var that = this;
        this.logovanje = {
            korisnicko_ime : "",
            lozinka : ""
        }
        this.uloga = "";
        this.state = {loggedIn: false};

        this.login = function(){
            $http.get("/login", {params: that.logovanje}).then(function(response){
                that.loggedIn = true;
                that.uloga = response.data;

                if (that.uloga == "administrator"){
                    $state.go("pica", {userName: that.logovanje.korisnicko_ime}, {reload: true});
                } else {
                    $state.go("sank", {userName: that.logovanje.korisnicko_ime}, {reload: true});
                };
            }, function() {
                alert("Neuspešna prijava!");
            })
        }
      
    }]);
})(angular);