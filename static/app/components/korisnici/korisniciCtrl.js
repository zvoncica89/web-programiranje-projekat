(function (angular) {
    var app = angular.module("app");
    
    app.controller("KorisniciCtrl", ["$http", "$state", function ($http, $state) {
        var that = this;
        this.korisnici = [];
        this.userName = $state.params.userName;

        this.uloge = []

        this.parametriPretrage = {
            korisnicko_ime : "",
            imePretraga : "",
            prezimePretraga : ""
        }

        this.noviKorisnik = {
            "korisnicko_ime": "",
            "ime": "",
            "prezime": "",
            "lozinka":"",
            "uloga":""
        }

        this.dobaviUloge = function(){
            $http.get("/uloge").then(function(response){
                that.uloge = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju uloga! Kod: " + response.status);
            })
        }

        this.dobaviKorisnike = function() {
            $http.get("/korisnici", {params: that.parametriPretrage}).then(function(response){
                that.korisnici = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju korisnika! Kod: " + response.status);
                $state.go("login",);
            })
        }

        this.clear = function(){
            that.parametriPretrage.korisnicko_ime = "";
            that.parametriPretrage.ime = "";
            that.parametriPretrage.prezime = "";
            $http.get("/korisnici", {params: that.parametriPretrage}).then(function(response){
                that.korisnici = response.data;                
            }, function(response) {
                console.log("Greska pri dobavljanju korisnika! Kod: " + response.status);
            })
        }

        this.ukloniKorisnika = function(korisnicko_ime) {
            $http.delete("/korisnici/"+korisnicko_ime).then(function(response){
                that.dobaviKorisnike();
            }, function(response){
                console.log("Greska pri uklanjanju korisnika! Kod: " + response.status);
            });
        }

        this.dodajKorisnika = function() {
            $http.post("/korisnici", that.noviKorisnik).then(function(response){
                that.dobaviKorisnike();
                that.clearKorisnika()
            }, function(){
                console.log("Greska pri dodavanju korisnika! Kod: " + response.status);
                window.alert("Korisnik sa ovim korisnickim imenom postoji!")
            });
        }
        this.logout = function() {
            $http.get("/logout/"+$state.params.userName).then(function(){
                $state.go("login", {}, {reload: true});
            }, function() {

            })
        }
        this.clearKorisnika = function(){
            this.noviKorisnik = {
                "korisnicko_ime": "",
                "ime": "",
                "prezime": "",
                "lozinka":"",
                "uloga":""
            }
        }
        this.dobaviKorisnike();
        this.dobaviUloge();
    }]);
})(angular);