var dobavljanje = [];
(function (angular) {
    var app = angular.module("app");
    
    app.controller("PicaCtrl", ["$http","$state", function ($http, $state) {
        var that = this;
        this.userName = $state.params.userName;
        this.pica = [];

        this.tip_pica = []

        this.novoPice = {
            "ime_pica" : "",
            "opis_pica" : "",
            "kolicina" : undefined,
            "cena" : undefined,
            "tip_pica_id_tipa_pica" : undefined
        };

        this.novaKategorija = {
            "ime_tipa" : ""
        };

        this.parametriPretrage = {
            ime_pica_pretraga : "",
            kolicinaDo: undefined,
            kolicinaOd: undefined,
            tip_pica_id_tipa_pica_pretraga: undefined
        }

        this.dobaviTipovePica = function() {
            $http.get("/pica/tip").then(function(response){
                that.tip_pica = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju tipova pica! Kod: " + response.status);
                $state.go("login");
            })
        }

        this.clear = function(){
            that.parametriPretrage.ime_pica_pretraga = "";
            that.parametriPretrage.kolicinaDo = undefined;
            that.parametriPretrage.kolicinaOd = undefined;
            that.parametriPretrage.tip_pica_id_tipa_pica_pretraga = undefined;
            $http.get("/pica", {params: that.parametriPretrage}).then(function(response){
                that.pica = response.data;                
            }, function(response) {
                console.log("Greska pri dobavljanju korisnika! Kod: " + response.status);
                $state.go("login");
            })
        }

        this.dobaviPica = function() {
            $http.get("/pica", {params: that.parametriPretrage}).then(function(response){
                that.pica = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju pica! Kod: " + response.status);
                $state.go("login");
            })
        }
       
        this.dodajPice = function() {
            $http.post("/pica", that.novoPice).then(function(response){
                that.dobaviPica();
                that.novoPice.ime_pica = "";
                that.novoPice.opis_pica = "";
                that.novoPice.kolicina = undefined;
                that.novoPice.cena = undefined;
                that.novoPice.tip_pica_id_tipa_pica = undefined;
            }, function(response){
                console.log("Greska pri dodavanju pica! Kod: " + response.status);
            });
        }

        this.dodajKategoriju = function() {
            $http.post("/pica/kategorija", that.novaKategorija).then(function(response){
                that.dobaviTipovePica()
                that.dobaviPica(); //Nakon uspesnog dodavanja dobavljaju se sve stavke sa servera.
                                   //Moguce je bilo i samo na klijentu dodati uspesno dodatu stavku.
            }, function(response){
                console.log("Greska pri dodavanju pica! Kod: " + response.status);
            });
        }

        this.ukloniPice = function(id_pica) {
            $http.delete("/pica/"+id_pica).then(function(response){
                that.dobaviPica();
            }, function(response){
                console.log("Greska pri uklanjanju pica! Kod: " + response.status);
            });
        }

        this.logout = function() {
            $http.get("/logout/"+$state.params.userName).then(function(){
                // that.loggedIn = false;
                $state.go("login", {}, {reload: true});
            }, function() {
            })
        }

        this.dobavljanje = function(){
            $http.get("/dobavljanje").then(function(response){
                if (confirm("Da li zelite da sacuvate narudzbenicu?")) {
                    $http.get("/stampanje").then(function(response){
                    }, function(response){
                        console.log("Greska pri stampanju porudzbenice! Kod: " + response.status);
                    });
                }
            }, function() {

            })
        }

        this.dobaviTipovePica();
        this.dobaviPica();
    }]);
})(angular);