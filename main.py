import flask
import datetime
from flask import Flask
from flask import request
from weasyprint import HTML

from flaskext.mysql import MySQL

from pymysql.cursors import DictCursor

mysql_db = MySQL(cursorclass=DictCursor)

app = Flask(__name__, static_url_path="")
app.secret_key = "ulogovan"

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'root'
app.config['MYSQL_DATABASE_DB'] = 'mydb'

mysql_db.init_app(app)
usernames = []

@app.route("/")
@app.route("/index")
def index():
    return app.send_static_file("index.html")

def provera_pristupa(uloga):
    dozvoli = False
    if len(usernames) > 0:
        for username in usernames:
            if flask.session.get(username) is not None and flask.session.get(username) == uloga:
                dozvoli = True
    if dozvoli:
        return True
    else:
        return False


@app.route("/login", methods=["GET"])
def login():
    data = dict()
    data["korisnicko_ime"] = request.args.get("korisnicko_ime")
    data["lozinka"] = request.args.get("lozinka")

    if not data["korisnicko_ime"] in usernames:
        cr = mysql_db.get_db().cursor()
        cr.execute("SELECT * FROM korisnici WHERE korisnicko_ime=%(korisnicko_ime)s AND lozinka=%(lozinka)s", data)
        user = cr.fetchone()
        cr.execute("SELECT * FROM prava_pristupa_has_korisnici")
        prava_pristupa = cr.fetchall()
        if user is not None:
            for i in range(0, len(prava_pristupa)):
                if prava_pristupa[i]["korisnici_id_korisnika"] == user["id_korisnika"]:
                    cr.execute("SELECT opis FROM prava_pristupa WHERE id="+str(prava_pristupa[i]["prava_pristupa_id"]))
                    user["prava"] = cr.fetchone()["opis"]
            flask.session[user["korisnicko_ime"]] = user["prava"]
            usernames.append(user["korisnicko_ime"])
            return user["prava"], 200
            # return "", 200
    
    return "", 401

@app.route("/logout/<userName>")
def logout(userName):
    # Prilikom odjave korisnik se brise iz sesije.
    flask.session.pop(userName, None)
    usernames.remove(userName)
    return "", 200

@app.route("/narucivanja", methods=["GET"])
def dobavljanje_tipova_pica():
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM tip_pica")
    tip_pica = cr.fetchall()
    return flask.json.jsonify(tip_pica)

@app.route("/porucivanje/<int:id_tipa_pica>", methods=["GET", "POST"])
def porucivanje_pica(id_tipa_pica):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM pice WHERE tip_pica_id_tipa_pica=%s", (id_tipa_pica, ))
    pica = cr.fetchall()
    return flask.json.jsonify(pica)

@app.route("/pica", methods=["GET"])
def dobavljanje_pica():
    if provera_pristupa("administrator"):
        cr = mysql_db.get_db().cursor()
        upit = ("SELECT * FROM pice")
        selekcija = " WHERE "
        parametri_pretrage = []
        if request.args.get("ime_pica_pretraga") is not None:
            parametri_pretrage.append("%" + request.args.get("ime_pica_pretraga") + "%")
            selekcija += "ime_pica LIKE %s "
        try:
            parametri_pretrage.append(int(request.args.get("kolicinaOd")))
            if len(parametri_pretrage) > 1:
                selekcija += "AND "
            selekcija += "kolicina >= %s "
        except:
            pass

        try:
            parametri_pretrage.append(int(request.args.get("kolicinaDo")))
            if len(parametri_pretrage) > 1:
                selekcija += "AND "
            selekcija += "kolicina <= %s "
        except:
            pass

        try:
            parametri_pretrage.append(int(request.args.get("tip_pica_id_tipa_pica_pretraga")))
            if len(parametri_pretrage) > 1:
                selekcija += "AND "
            selekcija += "tip_pica_id_tipa_pica = %s "
        except:
            pass

        if len(parametri_pretrage) > 0:
            upit += selekcija
        cr.execute(upit, parametri_pretrage)
        pica = cr.fetchall()
        cr.execute("SELECT * FROM tip_pica")
        tip_pica = cr.fetchall()
        for i in range(0, len(pica)):
            for j in range(0,len(tip_pica)):
                if pica[i]["tip_pica_id_tipa_pica"] == tip_pica[j]["id_tipa_pica"]:
                    pica[i]["tip_pica_id_tipa_pica"] = tip_pica[j]["ime_tipa"]
                    break
        return flask.json.jsonify(pica)    

@app.route("/pica/<int:id_pica>", methods=["GET"])
def dobavljanje_jednog_pica(id_pica):
    if provera_pristupa("administrator"):
        cr = mysql_db.get_db().cursor()
        cr.execute("SELECT * FROM pice WHERE id_pica=%s", (id_pica, ))
        pice = cr.fetchone()
        return flask.jsonify(pice)

@app.route("/pica/<int:id_pica>", methods=["DELETE"])
def uklanjanje_pica(id_pica):
    if provera_pristupa("administrator"):
        db = mysql_db.get_db()
        cr = db.cursor()
        cr.execute("DELETE FROM pice WHERE id_pica=%s", (id_pica,))
        db.commit()
        return "", 204

@app.route("/pica/tip", methods=["GET"])
def dobavljanje_tipova_pica2():
    if provera_pristupa("administrator"):
        cr = mysql_db.get_db().cursor()
        cr.execute("SELECT * FROM tip_pica")
        tip_pica = cr.fetchall()
        return flask.json.jsonify(tip_pica)

@app.route("/pica", methods=["POST"])
def dodavanje_pica():
    if provera_pristupa("administrator"):
        db = mysql_db.get_db()
        cr = db.cursor()
        cr.execute("INSERT INTO pice (ime_pica, opis_pica, kolicina, cena, tip_pica_id_tipa_pica) VALUES(%(ime_pica)s, %(opis_pica)s, %(kolicina)s, %(cena)s, %(tip_pica_id_tipa_pica)s)", request.json)
        db.commit()
        return "", 201

@app.route("/pica/kategorija", methods=["POST"])
def dodavanje_tipa_pica():
    if provera_pristupa("administrator"):
        db = mysql_db.get_db()
        cr = db.cursor()
        cr.execute("INSERT INTO tip_pica (ime_tipa) VALUES(%(ime_tipa)s)", request.json)
        db.commit()
        return "", 201

@app.route("/pice/<int:id_pica>", methods=["PUT"])
def izmena_pica(id_pica):
    if provera_pristupa("administrator"):
        db = mysql_db.get_db()
        cr = db.cursor()
        data = dict(request.json)
        data["id_pica"] = id_pica
        cr.execute("UPDATE pice SET ime_pica=%(ime_pica)s, opis_pica=%(opis_pica)s, kolicina=%(kolicina)s, cena=%(cena)s, tip_pica_id_tipa_pica=%(tip_pica_id_tipa_pica)s  WHERE id_pica=%(id_pica)s", data)
        db.commit()
        return "", 200

@app.route("/porudzbina", methods=["POST"])
def dodavanje_porudzbine():
    db = mysql_db.get_db()
    cr = db.cursor()
    narudzbina = dict()
    narudzbina["datum"] = datetime.datetime.now().replace(microsecond=0)
    narudzbina["status"] = 1
    narudzbina["id_uredjaja"] = 1
    narudzbina["ukupna_cena"] = request.json["ukupnaCena"]
    cr.execute("INSERT INTO narudzbina (datum, status, uredjaj_id_uredjaja, ukupna_cena) VALUES(%(datum)s, %(status)s, %(id_uredjaja)s, %(ukupna_cena)s)", narudzbina)
    datum = str(narudzbina["datum"]).split(".")
    narudzbina["datum"] = datum[0]
    cr.execute("SELECT id_narudzbine FROM narudzbina WHERE datum LIKE \"%" + narudzbina["datum"] + "%\"")
    id_porudzbine = cr.fetchone()
    for stavka in request.json["stavke"]:
        stavka["narudzbina_id_narudzbine"] = id_porudzbine["id_narudzbine"]
        cr.execute("INSERT INTO stavka_narudzbine (kolicina, pice_id_pica, cena, narudzbina_id_narudzbine) VALUES(%(kolicina)s, %(pice_id_pica)s, %(cena)s, %(narudzbina_id_narudzbine)s)", stavka)
    db.commit()
    return "", 201

@app.route("/sank/aktivnePorudzbine", methods=["GET"])
def dobavljanje_aktivnih_porudzbina():
    if provera_pristupa("konobar/sanker"):
        db = mysql_db.get_db()
        cr = db.cursor()
        cr.execute("SELECT id_narudzbine, ukupna_cena FROM narudzbina WHERE status=1")
        narudzbine = cr.fetchall()
        if len(narudzbine) > 0:
            for narudzbina in narudzbine:
                cr.execute("SELECT * FROM stavka_narudzbine WHERE narudzbina_id_narudzbine=%s", (narudzbina["id_narudzbine"],))
                narudzbina["stavke"] = cr.fetchall()
                for stavka in narudzbina["stavke"]:
                    cr.execute("SELECT ime_pica FROM pice WHERE id_pica=%s", (stavka["pice_id_pica"],))
                    stavka["ime_pica"] = cr.fetchone()["ime_pica"]
            return flask.json.jsonify(narudzbine), 201
        else:
            return "", 201

@app.route("/sank/<int:id>", methods=["DELETE"])
def izmena_porudzbine(id):
    if provera_pristupa("konobar/sanker"):
        db = mysql_db.get_db()
        cr = db.cursor()
        cr.execute("UPDATE narudzbina SET status=3 WHERE id_narudzbine="+str(id))
        db.commit()
        return "", 200

@app.route("/sank", methods=["PUT"])
def azuriranje_stavki():
    if provera_pristupa("konobar/sanker"):
        db = mysql_db.get_db()
        cr = db.cursor()
        cr.execute("SELECT id_korisnika FROM korisnici WHERE korisnicko_ime=\""+request.json["korisnik"]+"\"")
        korisnik = cr.fetchone()["id_korisnika"]
        cr.execute("UPDATE narudzbina SET status=2, korisnici_id_korisnika="+str(korisnik)+" WHERE id_narudzbine="+str(request.json["id_narudzbine"]))
        for stavka in request.json["stavke"]:
            cr.execute("SELECT kolicina FROM pice WHERE id_pica=%s", (stavka["pice_id_pica"],))
            novo_stanje = cr.fetchone()["kolicina"]-stavka["kolicina"]
            cr.execute("UPDATE pice SET kolicina=%s WHERE id_pica="+str(stavka["pice_id_pica"]), (novo_stanje,))
        db.commit()
        return "", 200

@app.route("/korisnici", methods=["GET"])
def dobavljanje_korisnika():
    if provera_pristupa("administrator"):
        cr = mysql_db.get_db().cursor()
        upit = "SELECT * FROM korisnici"
        selekcija = " WHERE "
        parametri_pretrage = []

        if request.args.get("korisnicko_ime") is not None:
            parametri_pretrage.append("%" + request.args.get("korisnicko_ime") + "%")
            selekcija += "korisnicko_ime LIKE %s "
        
        if request.args.get("imePretraga") is not None:
            parametri_pretrage.append("%" + request.args.get("imePretraga") + "%")
            if len(parametri_pretrage) > 1:
                selekcija += "AND "
            selekcija += "ime LIKE %s "
        
        if request.args.get("prezimePretraga") is not None:
            parametri_pretrage.append("%" + request.args.get("prezimePretraga") + "%")
            if len(parametri_pretrage) > 1:
                selekcija += "AND "
            selekcija += "prezime LIKE %s "

        if len(parametri_pretrage) > 0:
            upit += selekcija
        
        cr.execute(upit, parametri_pretrage)
        korisnici = cr.fetchall()
        return flask.json.jsonify(korisnici)

@app.route("/korisnici", methods=["POST"])
def dodavanje_korisnika():
    if provera_pristupa("administrator"):
        db = mysql_db.get_db()
        cr = db.cursor()
        cr.execute("INSERT INTO korisnici (korisnicko_ime, ime, prezime, lozinka) VALUES(%(korisnicko_ime)s, %(ime)s, %(prezime)s,  %(lozinka)s)", request.json)
        cr.execute("SELECT id_korisnika FROM korisnici WHERE korisnicko_ime=\""+request.json["korisnicko_ime"]+"\"")
        request.json["id_korisnika"] = cr.fetchone()["id_korisnika"]
        cr.execute("INSERT INTO prava_pristupa_has_korisnici (prava_pristupa_id, korisnici_id_korisnika) VALUES(%(uloga)s, %(id_korisnika)s)", request.json)
        db.commit()
        return "", 201

@app.route("/korisnici/<string:korisnicko_ime>", methods=["PUT"])
def izmena_korisnika(korisnicko_ime):
    if provera_pristupa("administrator"):
        db = mysql_db.get_db()
        cr = db.cursor()
        data = dict(request.json)
        data["korisnicko_ime"] = korisnicko_ime
        cr.execute("UPDATE korisnici SET ime=%(ime)s, prezime=%(prezime)s WHERE korisnicko_ime=%(korisnicko_ime)s", data)
        db.commit()
        return "", 200

@app.route("/korisnici/<string:korisnicko_ime>", methods=["GET"])
def dobavljanje_jednog_korisnika(korisnicko_ime):
    if provera_pristupa("administrator"):
        cr = mysql_db.get_db().cursor()
        cr.execute("SELECT * FROM korisnici WHERE korisnicko_ime=%s", (korisnicko_ime, ))
        korisnik = cr.fetchone()
        return flask.jsonify(korisnik)

@app.route("/korisnici/<string:korisnicko_ime>", methods=["DELETE"])
def uklanjanje_korisnika(korisnicko_ime):
    if provera_pristupa("administrator"):
        db = mysql_db.get_db()
        cr = db.cursor()
        cr.execute("DELETE FROM korisnici WHERE korisnicko_ime=%s", (korisnicko_ime,))
        db.commit()
        return "", 204

@app.route("/uloge", methods=["GET"])
def dobavljanje_uloga():
    if provera_pristupa("administrator"):
        cr = mysql_db.get_db().cursor()
        cr.execute("SELECT * FROM prava_pristupa")
        uloge = cr.fetchall()
        return flask.jsonify(uloge)

@app.route("/dobavljanje", methods=["GET"])
def dobavljanje_nabavke():
    if provera_pristupa("administrator"):
        cr = mysql_db.get_db().cursor()
        cr.execute("SELECT * FROM pice WHERE kolicina<=100")
        dobavljanje = cr.fetchall()
    return flask.jsonify(dobavljanje)
    # return flask.render_template("narudzbenica.tpl.html", dobavljanje=dobavljanje)

@app.route("/stampanje", methods=["GET"])
def stampanje():
    broj = 1
    HTML(filename='templates/narudzbenica.tpl.html').write_pdf("narudzbenica"+str(broj)+".pdf")
    broj+=1
    return ""


if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)